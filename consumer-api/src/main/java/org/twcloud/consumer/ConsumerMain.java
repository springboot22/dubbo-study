package org.twcloud.consumer;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.twcloud.api.model.entity.User;
import org.twcloud.api.model.service.UserService;

import java.io.IOException;

public class ConsumerMain {
    public static void main(String[] args) throws IOException, InterruptedException {
        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("applicationContext-consumer.xml");
        UserService userService = (UserService)classPathXmlApplicationContext.getBean("userService");
        User xiao = userService.login("xiao", "123");
        System.out.println("res:" + xiao);

//        new CountDownLatch(1).await();
    }
}
