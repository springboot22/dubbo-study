package org.twcloud.api.model.service;

import org.twcloud.api.model.entity.User;

public interface UserService {
    User login(String username, String password);
}
