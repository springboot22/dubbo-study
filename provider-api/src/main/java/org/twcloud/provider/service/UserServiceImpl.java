package org.twcloud.provider.service;

import org.springframework.stereotype.Service;
import org.twcloud.api.model.entity.User;
import org.twcloud.api.model.service.UserService;

@Service
public class UserServiceImpl implements UserService {
    @Override
    public User login(String username, String password) {
        return new User(username, password);
    }
}
